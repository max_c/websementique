package tp1.tp1;

import java.io.*;

import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.util.FileManager;

public class Requete1 {

	static final String inputFileVolcanoData = "volcanoData.ttl";
	static final String inputFileEarthquakeData = "EarthquakeData.ttl";
	static final String inputFileTsunamiData = "TsunamiData.ttl";
	static final String inputFileHapiness = "happiness.ttl";

	public static void main(String args[]) {
		Model modelHapiness = ModelFactory.createDefaultModel();
		InputStream inHapiness = FileManager.get().open(inputFileHapiness);
		if (inHapiness == null) {
			throw new IllegalArgumentException("File: " + inputFileHapiness + " not found");
		}
		modelHapiness.read(inHapiness, null, "Turtle");
		
		Model modelValcano = ModelFactory.createDefaultModel();
		InputStream inVolcano = FileManager.get().open(inputFileVolcanoData);
		if (inVolcano == null) {
			throw new IllegalArgumentException("File: " + inputFileVolcanoData + " not found");
		}
		modelValcano.read(inVolcano, null, "Turtle");

		Model modelTsunami = ModelFactory.createDefaultModel();
		InputStream inTsunami = FileManager.get().open(inputFileTsunamiData);
		if (inTsunami == null) {
			throw new IllegalArgumentException("File: " + inputFileTsunamiData + " not found");
		}
		modelTsunami.read(inTsunami, null, "Turtle");

		Model modelEarthquake = ModelFactory.createDefaultModel();
		InputStream inEarthquake = FileManager.get().open(inputFileEarthquakeData);
		if (inEarthquake == null) {
			throw new IllegalArgumentException("File: " + inputFileEarthquakeData + " not found");
		}
		modelEarthquake.read(inEarthquake, null, "Turtle");
		
		Model m = modelValcano.union(modelTsunami);
		Model m2 = m.union(modelEarthquake);
		Model m3 = m2.union(modelHapiness);


		String queryString = "PREFIX dbo: <http://dbpedia.org/ontology/> PREFIX  rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX locah:<http://data.archiveshub.ac.uk/def/> PREFIX dbpa:<http://dbpedia.org/page/> "
			+
				"SELECT ?country (COUNT(DISTINCT ?eruption) AS ?nbe)  (COUNT(DISTINCT ?tsunami) AS ?nbt) (COUNT(DISTINCT ?earthq) AS ?nbea)" + 
				"WHERE { " + 
					"?eruption rdf:type dbo:Eruption;" +
							"dbo:country ?country ." + 
					"?tsunami a dbpa:Tsunami; " +
						"dbo:country ?country."+
					"?earthq a <http://dbpedia.org/ontology#Earthquake>; " +
						"<http://dbpedia.org/ontology#locationCountry> ?country."+
				"} " + 
				"GROUP BY (?country)" + 
				"ORDER BY DESC (?nbe)" +
				"LIMIT 100" 
				;
		
	/*	String queryString = "PREFIX dbo: <http://dbpedia.org/ontology/> PREFIX  rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX locah:<http://data.archiveshub.ac.uk/def/> PREFIX dbpa:<http://dbpedia.org/page/> "
				+
					"SELECT DISTINCT ?country "+
					"WHERE { " + 
						"?eruption rdf:type dbo:Eruption;" +
								"dbo:country ?country ." + 
						"?tsunami a dbpa:Tsunami; " +
								"dbo:country ?country."+
						"?earthq a <http://dbpedia.org/ontology#Earthquake>; " +
								"<http://dbpedia.org/ontology#locationCountry> ?country."+
						"?happy <http://dbpedia.org/ontology/rank#happinessRank> ?x;" +
							"dbo:country ?countryHappy."+
						"FILTER regex(?country ,?countryHappy)."+

					"} " + 
					"GROUP BY (?country)" + 
					"ORDER BY (?country)" +
					"LIMIT 10" 
					;*/
		Query query = QueryFactory.create(queryString);
		try (QueryExecution qexec = QueryExecutionFactory.create(query, m3)) {
			ResultSet results = qexec.execSelect();
			ResultSetFormatter.out(System.out, results, query);
		}
	}
}